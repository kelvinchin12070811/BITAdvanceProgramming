
public class Cloth
{
    final int MAX_PRICE = 999; // final variable
    final int MIN_PRICE = 699;
    
    final void display() // final method
    {
        System.out.println("Max price is " + MAX_PRICE);
        System.out.println("Min price is " + MIN_PRICE);
    }
    
    public static void main(String[] args)
    {
        Cloth c1 = new Cloth();
        c1.display();
    }
}
