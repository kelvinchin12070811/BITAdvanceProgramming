import javax.swing.SwingUtilities;

public class Main
{
    public static void startAwtFrame(String[] args)
    {
        MyFrame frame = new MyFrame(); // Call MyFrame constructor
    }
    
    public static void startSwingFrame(String[] args)
    {
        SwingUtilities.invokeLater(new SwingFrame());
    }
    
    public static void main(String[] args)
    {
        //startSwingFrame(args); // Start swing version of ui
        startAwtFrame(args); // Start awt version of ui
    }

}
