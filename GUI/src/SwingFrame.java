import javax.swing.JFrame;

public class SwingFrame implements Runnable
{
    private JFrame frame = null;
    
    public SwingFrame()
    {
        frame = new JFrame();
        frame.setSize(300, 300);
        frame.setTitle("Swing frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void run()
    {
        frame.setVisible(true);
    }
}
