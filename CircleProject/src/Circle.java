
public class Circle
{
    public static final double PI = 3.1459;
    double radius = 5.0;
    
    public double findArea()
    {
        return (radius * radius) * Circle.PI;
    }
}
