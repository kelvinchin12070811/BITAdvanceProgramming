
public class RadiusModifierPrivate
{
    private double radius = 1.0;
    
    //Find the area of the radius
    public double getArea()
    {
        return radius * radius * Math.PI;
    }
    
    public static void main(String[] args)
    {
        RadiusModifierPrivate myCircle = new RadiusModifierPrivate();
        System.out.println("Radius is " + myCircle.radius);
    }
}
