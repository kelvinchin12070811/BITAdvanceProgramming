import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MainWindow extends JFrame
{
    private JButton btnCalculate = null;
    private JButton btnCancel = null;
    private JLabel labBmi = null;
    private JLabel labBmiResult = null;
    private JLabel labHeight = null;
    private JLabel labWeight = null;
    private JTextField txfBmi = null;
    private JTextField txfHeight = null;
    private JTextField txfWeight = null;

    public MainWindow()
    {
        initFrame();
        initComponents();
        initEvents();
        initLayout();
    }

    private void initFrame()
    {
        this.setTitle("BMI Calculator");
        this.setSize(new Dimension(300, 200));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    private void initComponents()
    {
        btnCalculate = new JButton("Calculate");
        btnCancel = new JButton("Cancel");

        labBmi = new JLabel("Your BMI");
        labBmiResult = new JLabel();
        labHeight = new JLabel("Your Height");
        labWeight = new JLabel("Your Weight");

        txfBmi = new JTextField();
        txfWeight = new JTextField();
        txfHeight = new JTextField();

        labBmiResult.setVerticalAlignment(SwingConstants.TOP);
        labBmiResult.setHorizontalAlignment(SwingConstants.CENTER);

        txfBmi.setEditable(false);
    }

    private void initEvents()
    {
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                onBtnCalculate_clicked(e);
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                onBtnCancel_clicked(e);
            }
        });
    }

    private void initLayout()
    {
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints def = new GridBagConstraints();
        def.fill = GridBagConstraints.BOTH;
        def.insets = new Insets(3, 3, 3, 3);

        def.gridx = 0;
        def.gridy = 0;
        def.gridwidth = 1;
        def.gridheight = 1;
        def.weightx = 0;
        layout.setConstraints(labHeight, def);

        def.gridx = 1;
        def.gridy = 0;
        def.gridwidth = 3;
        def.gridheight = 1;
        def.weightx = 1.0;
        layout.setConstraints(txfHeight, def);

        def.gridx = 0;
        def.gridy = 1;
        def.gridwidth = 1;
        def.gridheight = 1;
        def.weightx = 0;
        layout.setConstraints(labWeight, def);

        def.gridx = 1;
        def.gridy = 1;
        def.gridwidth = 3;
        def.gridheight = 1;
        def.weightx = 1.0;
        layout.setConstraints(txfWeight, def);

        def.gridx = 0;
        def.gridy = 2;
        def.gridwidth = 1;
        def.gridheight = 1;
        def.weightx = 0;
        layout.setConstraints(labBmi, def);

        def.gridx = 1;
        def.gridy = 2;
        def.gridwidth = 3;
        def.gridheight = 1;
        def.weightx = 1.0;
        layout.setConstraints(txfBmi, def);

        def.gridx = 0;
        def.gridy = 3;
        def.gridwidth = 2;
        def.gridheight = 1;
        def.weightx = 1.0;
        layout.setConstraints(btnCalculate, def);

        def.gridx = 2;
        def.gridy = 3;
        def.gridwidth = 2;
        def.gridheight = 1;
        def.weightx = 1.0;
        layout.setConstraints(btnCancel, def);

        def.gridx = 0;
        def.gridy = 4;
        def.gridwidth = 4;
        def.gridheight = 1;
        def.weightx = 1.0;
        def.weighty = 1.0;
        layout.setConstraints(labBmiResult, def);

        this.add(labHeight);
        this.add(txfHeight);
        this.add(labWeight);
        this.add(txfWeight);
        this.add(labBmi);
        this.add(txfBmi);
        this.add(btnCalculate);
        this.add(btnCancel);
        this.add(labBmiResult);
    }

    private void onBtnCalculate_clicked(ActionEvent ev)
    {
        try
        {
            double bmi = 0;
            double height = Double.parseDouble(txfHeight.getText());
            double weight = Double.parseDouble(txfWeight.getText());
            
            bmi = weight / Math.pow(height, 2);
            txfBmi.setText(String.valueOf(bmi));
            
            if (bmi <= 18.5)
                labBmiResult.setText("Underweight");
            else if (bmi > 18.5 && bmi <= 24.99)
                labBmiResult.setText("Normal Weight");
            else if (bmi > 24.99 && bmi <= 29.99)
                labBmiResult.setText("Overweight");
            else if (bmi > 29.99 && bmi <= 34.99)
                labBmiResult.setText("Obesity class 1");
            else if (bmi > 34.99 && bmi <= 39.99)
                labBmiResult.setText("Obesity class 2");
            else if (bmi > 39.99)
                labBmiResult.setText("Obesity class 3");
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void onBtnCancel_clicked(ActionEvent e)
    {
        txfBmi.setText("");
        txfHeight.setText("");
        txfWeight.setText("");
        labBmiResult.setText("");
    }
}
