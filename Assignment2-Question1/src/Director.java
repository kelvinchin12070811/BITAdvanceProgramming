import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;

import javax.swing.JTextField;

public class Director implements Runnable
{
    private boolean running = false;
    private BufferStrategy bs = null;
    private Graphics graphics = null;
    private double deltaTime = 0;
    private int width = 0;
    private int height = 0;
    private MainFrame frame = null;
    private String title = null;
    private Thread thread = null;
    private Car[] cars = null;
    
    public static Director getInstance()
    {
        return DirectorInstance.instance;
    }
    
    public double getDeltaTime()
    {
        return deltaTime;
    }
    
    public void init(String title, int width, int height)
    {
        this.title = title;
        this.width = width;
        this.height = height;
    }
    
    public void createFrame()
    {
        frame = new MainFrame(title, width, height);
        frame.setVisible(true);
        
        cars = new Car[4];
        for (int itr = 0; itr < cars.length; itr++)
            cars[itr] = new Car(0, itr, width);
        
        JTextField[] speedCtrlers = frame.getCarSpeedControllers();
        
        for (int i = 0; i < speedCtrlers.length; i++)
        {
            final int idx = i;
            speedCtrlers[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    updateSpeed(cars[idx], speedCtrlers[idx].getText());
                }
            });
        }
    }

    @Override
    public void run()
    {
        createFrame();
        long prevTime = System.nanoTime();
        long prevFrameTime = System.nanoTime();
        long curTime = 0L;
        double timePerFrame = 1000000000.0 / 60.0;
        double framesToProcess = 0.0;
        while (running)
        {
            curTime = System.nanoTime();
            framesToProcess += (curTime - prevTime) / timePerFrame;
            prevTime = curTime;
            if (framesToProcess >= 1.0)
            {
                deltaTime = (curTime - prevFrameTime) * 1e-9;
                prevFrameTime = curTime;
                tick();
                draw();
                framesToProcess--;
            }
        }
        stop();
    }

    public synchronized void start()
    {
        if (running) return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop()
    {
        if (!running) return;
        running = false;
        try
        {
            thread.join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private Director() {} // Make Director singleton
    
    private void tick()
    {
        for (Car itr : cars)
            itr.tick();
    }

    private void draw()
    {
        bs = frame.getCanvas().getBufferStrategy();
        if (bs == null)
        {
            frame.getCanvas().createBufferStrategy(3);
            return;
        }

        graphics = bs.getDrawGraphics();
        graphics.clearRect(0, 0, width, height);
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, width, height);
        graphics.setColor(Color.BLACK);
        // Draw here

        graphics.drawRect(0, 0, width, height);

        final int spacing = height / 4;
        for (int i = 1; i <= 3; i++)
        {
            graphics.drawLine(0, i * spacing, width, i * spacing);
        }

        for (Car itr : cars)
            itr.draw(graphics);

        // End draw
        bs.show();
        graphics.dispose();
    }

    private void updateSpeed(Car target, String nSpeed)
    {
        int value = 1;
        try
        {
            value = Integer.parseInt(nSpeed);
        }
        catch (Exception e)
        {
            value = 1;
        }
        target.setSpeed(value);
    }
    
    // Thread safe lazy initialization
    private static class DirectorInstance
    {
        public static final Director instance = new Director();
    }
}
