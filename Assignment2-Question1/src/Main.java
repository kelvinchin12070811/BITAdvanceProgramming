
public class Main
{
    public static void main(String[] args)
    {
        Director director = Director.getInstance();
        director.init("Car Racing Simulator", 640, 480);
        director.start();
    }
}
