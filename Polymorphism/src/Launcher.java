
public class Launcher
{
    public static void main(String[] args)
    {
        int i = 10;
        int j = 5;
        int k;

        String x = "John";
        String y = " Smith";
        String z;

        int c = add(i, j);
        z = add(x, y);

        System.out.println(c + " " + z);
    }

    public static int add(int a, int b)
    {
        return a + b;
    }

    public static String add(String a, String b)
    {
        String c = a + b;
        return c;
    }
}
