
public class Test
{
    double width;
    double height;
    
    public Test()
    {
        width = 1.0;
        height = 1.0;
    }
    
    public Test(double width, double height)
    {
        this.width = width;
        this.height = height;
    }
    
    public double getArea()
    {
        return width * height;
    }
    
    public double getPerimeter()
    {
        return (width * 2) + (height * 2);
    }
}
