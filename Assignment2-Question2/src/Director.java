import java.awt.Canvas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JTextField;

public class Director implements Runnable
{
    private volatile boolean running = false;
    private int height = 0;
    private int width = 0;
    private MainFrame frame = null;
    private String title = null;
    private Thread thread = null;
    
    private Car[] cars = null;
    public static Director getInstance()
    {
        return DirectorInstance.instance;
    }
    
    public void init(String title, int width, int height)
    {
        this.title = title;
        this.height = height;
        this.width = width;
    }
    
    public boolean isRunning()
    {
        return running;
    }
    
    public void run()
    {
        createFrame();
    }
    
    public synchronized void start()
    {
        if (running) return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }
    
    public synchronized void stop()
    {
        if (!running) return;
        running = false;
        try
        {
            for (Car itr : cars)
                itr.stop();
            
            thread.join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
    
    private Director() {} // Mark as singleton class
    
    private void createFrame()
    {
        frame = new MainFrame(title, width, height);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e)
            {
                Director.getInstance().stop();
                super.windowClosing(e);
            }
        });
        
        cars = new Car[4];
        Canvas[] canvases = frame.getCanvases();
        JTextField[] carSpeedCtrls = frame.getSpeedController(); 
        for (int i = 0; i < cars.length; i++)
        {
            final int idx = i; // actionListener require a constant value of i;
            cars[i] = new Car(i, canvases[i]);
            carSpeedCtrls[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    updateSpeed(cars[idx], carSpeedCtrls[idx].getText());
                }
            });
            cars[i].start();
        }
    }
    
    private void updateSpeed(Car target, String nSpeed)
    {
        int value = 0;
        try
        {
            value = Integer.parseInt(nSpeed);
        }
        catch (NumberFormatException e1)
        {
            value = 1;
        }
        target.setSpeed(value);
    }
    
    private static class DirectorInstance // Avoid data race while access
    {
        public static final Director instance = new Director();
    }
}
