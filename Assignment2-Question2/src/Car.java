import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Car implements Runnable
{
    public static final double MAX_SPEED = 1000.0;
    public static final int TEXTURE_PADDING = 15;

    private volatile boolean running = false;
    private BufferedImage texture = null;
    private BufferStrategy bs = null;
    private Canvas canvas = null;
    private volatile int speed = 1;
    private double deltaTime = 0;
    private double x = 0.0;
    private double y = 0.0;
    private Graphics graphics = null;
    private Thread thread = null;

    public Car(double row, Canvas canvas)
    {
        y = (double) TEXTURE_PADDING;
        texture = AssetsLoader.getCarTexture();
        this.canvas = canvas;
    }

    @Override
    public void run()
    {
        long prevTime = System.nanoTime();
        long prevFrameTime = prevTime;
        long curTime = 0l;
        double timePerFrame = 1000000000.0 / 60.0;
        System.out.printf("delay time: %sns\n", timePerFrame);
        double framesToProcess = 0.0;

        while (running)
        {
            curTime = System.nanoTime();
            framesToProcess += (curTime - prevTime) / timePerFrame;
            prevTime = curTime;
            if (framesToProcess >= 1.0)
            {
                deltaTime = (curTime - prevFrameTime) * 1e-9;
                prevFrameTime = curTime;
                tick();
                draw();
                framesToProcess--;
            }
            if (!running) System.out.printf("Endding thread %s\n", thread.getId());
        }
        stop();
        System.out.printf("Thread %s stopped\n", thread.getId());
    }

    public void setSpeed(int value)
    {
        speed = Math.abs(value);
    }

    public synchronized void start()
    {
        if (running) return;
        running = true;
        thread = new Thread(this);
        System.out.printf("Starting thread %s\n", thread.getId());
        thread.start();
    }

    public void stop()
    {
        if (!running) return;
        synchronized (this)
        {
            System.out.printf("Stopping thread %s\n", thread.getId());
            running = false;
        }
        try
        {
            thread.join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    private void tick()
    {
        if (x >= canvas.getWidth())
            x = -texture.getWidth();
        else
            x += 1.0 * deltaTime * (MAX_SPEED / speed);
    }

    private void draw()
    {
        bs = canvas.getBufferStrategy();
        if (bs == null)
        {
            canvas.createBufferStrategy(3);
            return;
        }
        graphics = bs.getDrawGraphics();

        graphics.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        graphics.setColor(Color.BLACK);
        graphics.drawRect(0, 0, canvas.getWidth(), canvas.getHeight());
        graphics.drawImage(texture, (int) x, (int) y, null);

        bs.show();
        graphics.dispose();
    }
}
